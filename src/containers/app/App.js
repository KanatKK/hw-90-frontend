import React, {useState, useRef, useEffect} from 'react';
import './App.css';

const App = () => {
    const [state, setState] = useState({
        mouseDown: false,
        pixelsArray: []
    });
    const canvas = useRef(null);

    useEffect(() => {
        canvas.current = new WebSocket("ws://localhost:8000/canvas");
        canvas.current.onopen = () => {
            canvas.current.send(JSON.stringify({type: "GET_ALL_PIXELS"}));
        };
        canvas.current.onclose = () => console.log("ws connection closed");
        canvas.current.onmessage = e => {
            const decodedMessage = JSON.parse(e.data);
            if (decodedMessage.type === "ALL_PIXELS") {
                for (const key in decodedMessage.pixelsArray) {
                    for (const item in decodedMessage.pixelsArray[key].pixelsArray) {
                        setState(
                            prevState => {
                                return {
                                    ...prevState,
                                    pixelsArray: [...prevState.pixelsArray, {
                                        x: decodedMessage.pixelsArray[key].pixelsArray[item].x,
                                        y: decodedMessage.pixelsArray[key].pixelsArray[item].y,
                                    }]
                                };
                            }
                        );
                    }
                }
            } else if (decodedMessage.type === "NEW_PIXELS") {
                for (const key in decodedMessage.pixelsArray) {
                    for (const item in decodedMessage.pixelsArray[key].pixelsArray) {
                        setState(
                            prevState => {
                                return {
                                    ...prevState,
                                    pixelsArray: [...prevState.pixelsArray, {
                                        x: decodedMessage.pixelsArray[key].pixelsArray[item].x,
                                        y: decodedMessage.pixelsArray[key].pixelsArray[item].y,
                                    }]
                                };
                            }
                        );
                    }
                }
            }
        };
        return () => canvas.current.close();
    }, []);

    useEffect(() => {
        if (state.pixelsArray[0] !== undefined) {
            const myCanvas = document.querySelector("canvas");
            const context = myCanvas.getContext('2d');
            for (let i = 0; i <= state.pixelsArray.length - 2; i++) {
                for (let k = i + 1; k <= state.pixelsArray.length - 1; k++) {
                    context.moveTo(state.pixelsArray[i].x, state.pixelsArray[i].y);
                    context.lineTo(state.pixelsArray[k].x, state.pixelsArray[k].y);
                }
            }
            context.stroke();
        }
    }, [state.pixelsArray]);

    const canvasMouseMoveHandler = event => {
        if (state.mouseDown) {
            event.persist();
            const clientX = event.clientX;
            const clientY = event.clientY;
            setState(prevState => {
                return {
                    ...prevState,
                    pixelsArray: [...prevState.pixelsArray, {
                        x: clientX,
                        y: clientY,
                    }]
                };
            });

            const myCanvas = document.querySelector("canvas");
            const context = myCanvas.getContext('2d');
            const imageData = context.createImageData(1, 1);
            const d = imageData.data;

            d[0] = 0;
            d[1] = 0;
            d[2] = 0;
            d[3] = 255;

            context.putImageData(imageData, event.clientX, event.clientY);
        }
    };
    const mouseDownHandler = event => {
        setState({...state, mouseDown: true});
    };

    const mouseUpHandler = event => {
        canvas.current.send(JSON.stringify({
            type: "CREATE_PIXELS",
            pixelsArray: state.pixelsArray,
        }))
        setState({...state, mouseDown: false, pixelsArray: []});
    };

  return (
      <div className="container">
          <canvas
              ref={canvas}
              style={{border: '1px solid black'}}
              width={500}
              height={500}
              onMouseDown={mouseDownHandler}
              onMouseUp={mouseUpHandler}
              onMouseMove={canvasMouseMoveHandler}
          />
      </div>
  );
};

export default App;
